Catalyst
========

Catalyst is an **in-situ framework** created as part of ParaView a few
years ago. With time and use, we saw the different drawbacks of our
approach and then it was decided to create a **new architecture**,
`Catalyst API`_.

.. _Catalyst API: https://catalyst-in-situ.readthedocs.io/en/latest/index.html

With this new implementation, it is:

* **easier to implement** in your simulation (less knowledge required)
* **easier to update** from a version to another (less dependencies, has binary compatibility)
* possible to activate **Steering** mode, where ParaView can modify simulation parameters at runtime

.. note::
  This is documentation for the Paraview implementation of the Catalyst API (Catalyst
  2) if you are looking for information regarding the previous version of
  catalyst check this manual_.

.. _manual: https://www.paraview.org/files/catalyst/docs/ParaViewCatalystUsersGuide_v2.pdf

.. toctree::
   :hidden:

   getting_started
   background
   blueprints
